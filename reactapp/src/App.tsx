import { useEffect, useState } from "react";
import reactLogo from "./assets/react.svg";
import { DateTime } from "luxon";

import styled from "styled-components";

function App() {
  const [events, setEvents] = useState([]);
  const [activities, setActivities] = useState([]);

  const [filter, setFilter] = useState("dsc");

  useEffect(() => {
    fetch("http://recruitment.golem.network:16655/events")
      .then((response) => response.json())
      .then((data) => setEvents(data.events));

    fetch("http://recruitment.golem.network:16655/activities")
      .then((response) => response.json())
      .then((data) => {
        // normalize in place
        for (let index = 0; index < data.activities.length; index++) {
          data.activities[index].start = DateTime.fromISO(
            data.activities[index].start
          );
          data.activities[index].end = DateTime.fromISO(
            data.activities[index].end
          );
        }

        const sortFn = (a: Activity, b: Activity) => {
          return a.start.toMillis() - b.start.toMillis();
        };

        setActivities(data.activities.sort(sortFn));
      });
  }, []);

  let filteredActivities: Array<Activity> = [];

  let isAddedEvent: Array<Number> = [];

  activities.forEach((singleActivity: Activity) => {
    let eventsInActivity: Array<MyEvent> = [];

    events.forEach((singleEvent: MyEvent, index) => {
      const eventTime = DateTime.fromISO(singleEvent.date);

      if (
        eventTime >= singleActivity.start &&
        eventTime <= singleActivity.end
      ) {
        eventsInActivity.push(singleEvent);
        isAddedEvent.push(index);
      }
    });

    if (eventsInActivity.length > 0) {
      filteredActivities.push({
        ...singleActivity,
        eventsInActivity: eventsInActivity,
      });
    }
  });

  let orphanedEvents: Array<MyEvent> = [];
  for (let i = 0; i < events.length; i++) {
    if (!isAddedEvent.includes(i)) {
      orphanedEvents.push(events[i]);
    }
  }

  // console.log(filteredActivities);
  // console.log(orphanedEvents);

  if (filter == "dsc") {
    filteredActivities = filteredActivities.sort((a, b) => {
      // @ts-ignore
      return b.eventsInActivity.length - a.eventsInActivity.length;
    });
  } else {
    filteredActivities = filteredActivities.sort((a, b) => {
      // @ts-ignore
      return a.eventsInActivity.length - b.eventsInActivity.length;
    });
  }

  return (
    <ActivityContainer>
      <button
        onClick={() => {
          setFilter("dsc");
        }}
      >
        Od największej
      </button>
      <button
        onClick={() => {
          setFilter("asc");
        }}
      >
        Od najmniejszej
      </button>
      {filteredActivities.map((singleActivity) => (
        <ActivityComponent data={singleActivity} />
      ))}
    </ActivityContainer>
  );
}

export default App;

// @ts-ignore
const ActivityComponent = ({ data }) => {
  return (
    <StyledActivityComponent>
      <div>
        <h2>{data.name}</h2>
        <small>{data.id}</small>
      </div>
      <DatesContainer>
        <div>
          {data.start.setLocale("pl").toLocaleString(DateTime.DATE_FULL)}
        </div>
        <div>{data.end.setLocale("pl").toLocaleString(DateTime.DATE_FULL)}</div>
      </DatesContainer>

      {/* @ts-ignore */}
      {data.eventsInActivity.map((e) => (
        <EventComponent data={e} />
      ))}
    </StyledActivityComponent>
  );
};

// @ts-ignore
const EventComponent = ({ data }) => {
  return (
    <StyledEventComponent>
      {data.name} {" "}
      {data.id} {" "}
      {data.start} {" "}
    </StyledEventComponent>
  );
};

const ActivityContainer = styled.div`
  margin: 5% 15%;
`;

const StyledActivityComponent = styled.div`
  background-color: #a7a7a7;
  padding: 10px;
  border-bottom: solid 1px black;
  margin: 5px;
  /* width: 100%; */
`;

const DatesContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const StyledEventComponent = styled.div`
  background-color: #efefef;
`;

interface Activity {
  id: number;
  name: string;
  start: DateTime;
  end: DateTime;
  eventsInActivity?: Array<MyEvent>;
}

interface MyEvent {
  id: number;
  name: string;
  date: string;
}
